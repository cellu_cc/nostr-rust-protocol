# Slightly Modified Nostr Protocol for Tabletop-Engine

This is supposed to be the import crate for the client/server on Tabletop Engine
This gives you the structs to create and sign Nostr messages to send to the server, and

## Client/Server Narrative
Client requests a new game with public keys of all players, and a combined army list
Server replies with UUID of new game
Client requests to join the new game
Once all clients have joined, Server sends a notification that phase is changing to Objective
Clients may then send "place objective" messages, the server then verifies if the placed objective is in a valid location, and returns with a "place objective" message if successful




