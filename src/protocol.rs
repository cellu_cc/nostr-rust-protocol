#![allow(dead_code)]

use ed25519_dalek::PublicKey;
use uuid::Uuid;
use std::collections::HashMap;


pub struct Game{
    id: Uuid
  , current_player: Option<PublicKey>
  , players: Vec<PublicKey>
  , current_phase: GamePhase
  , models: HashMap<Uuid, Model>
  }

#[derive(Clone)]
pub struct Model{
    name: String
  , player: PublicKey
  , location: Option<[u32; 2]>
}

pub enum ServerMessage{
    ChangePhase(GamePhase)
}

pub enum PlayerMessage{
    NewGame
}

pub enum GamePhase{
    Setup
  , Objective
  , Deployment
  , Combat
}

impl Game{
    pub fn new(players: Vec<PublicKey>, models: HashMap<Uuid, Model>) -> Self{
        Self {
            id: Uuid::new_v4()
          , players: players.clone()
          , current_player: None
          , current_phase: GamePhase::Setup
          , models: models.clone()
        }
    }
}

