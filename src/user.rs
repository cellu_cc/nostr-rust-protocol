use ed25519_dalek::{
    Keypair
  , PublicKey
  , SecretKey
  };

use rand::rngs::OsRng;

use hex;

use thiserror::Error;

#[derive(Error, Debug, PartialEq)]
pub enum KeyError {
    #[error("Invalid secret key string")]
    SkParseError,
    #[error("Invalid public key string")]
    PkParseError,
    #[error("Secrete key missing")]
    SkMissing,
    #[error("Key pair missing")]
    KeyPairMissing,
    #[error("Failed to generate new keys")]
    KeyGenerationFailure,
}

#[derive(Debug)]
pub struct Keys {
    pub public_key: PublicKey,
    key_pair: Option<Keypair>,
    secret_key: Option<SecretKey>,
}

impl Keys {
    pub fn generate_from_os_random() -> Result<Self, KeyError> {
        let mut rng = OsRng{};
        let keypair: Keypair = Keypair::generate(&mut rng);
        Self::new(&keypair.to_bytes())
    }

    pub fn new_pub_only(pk: &[u8]) -> Result<Self, KeyError> {
        let pk = PublicKey::from_bytes(pk).map_err(|_| KeyError::PkParseError)?;

        Ok(Keys {
            public_key: pk,
            key_pair: None,
            secret_key: None,
        })
    }

    pub fn new(kp: &[u8]) -> Result<Self, KeyError> {
        let key_pair = Keypair::from_bytes(kp).map_err(|_| KeyError::PkParseError)?;
        
        Ok(Self {
            public_key: key_pair.public,
            key_pair: Some(Keypair::from_bytes(&key_pair.to_bytes()).unwrap()),
            secret_key: Some(key_pair.secret),
        })
    }

    pub fn public_key_as_str(&self) -> String {
        hex::encode(self.public_key.to_bytes())
    }

    pub fn secret_key(&self) -> Result<SecretKey, KeyError> {
        if let Some(secret_key) = &self.secret_key {
            Ok(SecretKey::from_bytes(&secret_key.to_bytes()).unwrap())
        } else {
            Err(KeyError::SkMissing)
        }
    }

    pub fn secret_key_as_str(&self) -> Result<String, KeyError> {
        if let Some(secret_key) = &self.secret_key {
            Ok(hex::encode(secret_key.to_bytes()))
        } else {
            Err(KeyError::SkMissing)
        }
    }

    pub fn key_pair(&self) -> Result<Keypair, KeyError> {
        if let Some(key_pair) = &self.key_pair {
            Ok(Keypair::from_bytes(&key_pair.to_bytes()).unwrap())
        } else {
            Err(KeyError::KeyPairMissing)
        }
    }
}

#[cfg(test)]
mod test{
    use super::*;

    #[test]
    fn test_new_from_osrng() {
        let keys = Keys::generate_from_os_random().unwrap();
        print!("Public Key: {:?}", keys.public_key_as_str());
        print!("Secret Key: {:?}", keys.secret_key_as_str().unwrap());
    }

    #[test]
    fn test_new_from_bytes() {
        let keypair_str = hex::decode("35d8fa2be510c9829501ea5bebffd89c6ab15a06eb1aea8ec79c6fffe1f62b085e522d9b168b5c0abeb8d1d164a66095f7142051ae044de7ed2d36e516cdd7c8").unwrap();
        let kp = Keys::new(&keypair_str[..]).unwrap();
        assert_eq!(kp.secret_key_as_str().unwrap(), "35d8fa2be510c9829501ea5bebffd89c6ab15a06eb1aea8ec79c6fffe1f62b08");
        assert_eq!(kp.public_key_as_str(), "5e522d9b168b5c0abeb8d1d164a66095f7142051ae044de7ed2d36e516cdd7c8");
    }
}
