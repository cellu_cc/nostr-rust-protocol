use crate::Keys;
use chrono::{serde::ts_seconds, Utc};
use chrono::{DateTime, NaiveDateTime};
use serde::{Deserialize, Deserializer, Serialize};
use serde_json::json;
use serde_repr::*;
use std::{
    error::Error,
    str::FromStr,
    time::{Duration, SystemTime, UNIX_EPOCH},
};
use ed25519_dalek::{PublicKey, Signature, Signer, SignatureError, Verifier};
use sha2::Digest;
use hex;

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct Event {
    pub id: [u8;32], // hash of serialized event with id 0
    pub pubkey: PublicKey,
    #[serde(with = "ts_seconds")]
    pub created_at: DateTime<Utc>, // unix timestamp seconds
    pub kind: Kind,
    pub tags: Vec<Tag>,
    pub content: String,
    #[serde(deserialize_with = "sig_string")] // Serde derive is being weird
    pub sig: ed25519_dalek::Signature,
}

fn sig_string<'de, D>(deserializer: D) -> Result<ed25519_dalek::Signature, D::Error>
where
    D: Deserializer<'de>,
{
    let s: String = Deserialize::deserialize(deserializer)?;
    let sig = ed25519_dalek::Signature::from_str(&s);
    sig.map_err(serde::de::Error::custom)
}

impl Event {
    fn gen_id(
        pubkey: &ed25519_dalek::PublicKey,
        created_at: &DateTime<Utc>,
        kind: &Kind,
        tags: &Vec<Tag>,
        content: &str,
    ) -> [u8;32] {
        let event_json =
            json!([0, pubkey, created_at.timestamp(), kind, tags, content]).to_string();
        let mut digest = sha2::Sha256::new();
        digest.update(event_json);
        digest.finalize().into()
    }

    fn time_now() -> DateTime<Utc> {
        // Doing all this extra work to construct a DateTime with zero nanoseconds
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .expect("system time before Unix epoch");
        let naive = NaiveDateTime::from_timestamp(now.as_secs() as i64, 0);
        DateTime::from_utc(naive, Utc)
    }
    /// Create a new TextNote Event
    pub fn new_textnote(
        content: &str,
        keys: &Keys,
        // keypair: &ed25519::KeyPair,
    ) -> Result<Self, Box<dyn Error>> {

        let keypair = &keys.key_pair()?;

        let pubkey = keypair.public;
        dbg!(hex::encode(pubkey));

        let created_at = Self::time_now();

        // TODO: support more event kinds
        let kind = Kind::TextNote;

        // For some reason the timestamp isn't serializing correctly so I do it manually
        let id = Self::gen_id(&pubkey, &created_at, &kind, &vec![], content);

        // Let the schnorr library handle the aux for us
        // I _think_ this is bip340 compliant
        let sig = keypair.sign(&id);

        let event = Event {
            id,
            pubkey,
            created_at,
            kind,
            tags: vec![],
            content: content.to_string(),
            sig,
        };

        // This isn't failing so that's a good thing, yes?
        match event.verify() {
            Ok(()) => Ok(event),
            Err(e) => Err(Box::new(e)),
        }
    }
/*     pub fn new_encrypted_direct_msg(
        sender: &Keys,
        receiver: &Keys,
        // sender_sk: SecretKey,
        // receiver_pk: &schnorrsig::PublicKey,
        content: &str,
    ) -> Result<Self, Box<dyn Error>> {
        let secp = Secp256k1::new();
        // let sender_keypair = schnorrsig::KeyPair::from_secret_key(&secp, sender.secret_key());
        // let sender_pk = schnorrsig::PublicKey::from_keypair(&secp, &sender_keypair);

        let encrypted_content =
            nip04::encrypt(&sender.secret_key()?, &receiver.public_key, content);
        let kind = Kind::EncryptedDirectMessage;
        let created_at = Self::time_now();
        let tags = vec![Tag::new("p", &receiver.public_key_as_str(), "")];
        // TODO maybe could pass Keys here
        let id = Self::gen_id(
            &sender.public_key,
            &created_at,
            &kind,
            &tags,
            &encrypted_content,
        );

        let id_to_sign = secp256k1::Message::from(id);

        let sig = secp.schnorrsig_sign(&id_to_sign, &sender.key_pair()?);

        Ok(Event {
            id,
            pubkey: sender.public_key,
            created_at,
            kind,
            tags,
            content: encrypted_content,
            sig,
        })
    }
 */
    pub fn verify(&self) -> Result<(), SignatureError> {
        let id = Self::gen_id(
            &self.pubkey,
            &self.created_at,
            &self.kind,
            &self.tags,
            &self.content,
        );
        self.pubkey.verify(&id, &self.sig)
    }

    /// This is just for serde sanity checking
    #[allow(dead_code)]
    pub(crate) fn new_dummy(
        id: &str,
        pubkey: &str,
        created_at: u32,
        kind: u8,
        tags: Vec<Tag>,
        content: &str,
        sig: &str,
    ) -> Result<Self, Box<dyn Error>> {
        let mut id_bytes = [0u8; 32];
        if ! hex::decode_to_slice(id, &mut id_bytes as &mut [u8]).is_ok() { return Err("Error Decoding ID".into()) };
        let pubkey = PublicKey::from_bytes(&hex::decode(pubkey)?[..])?;
        let created_at = DateTime::<Utc>::from(UNIX_EPOCH + Duration::new(created_at as u64, 0));
        let kind = serde_json::from_str(&kind.to_string())?;
        let sig = Signature::from_str(sig)?;

        let event = Event {
            id: id_bytes,
            pubkey,
            created_at,
            kind,
            tags,
            content: content.to_string(),
            sig,
        };

        if event.verify().is_ok() {
            Ok(event)
        } else {
            Err("Didn't verify".into())
        }
    }

    pub fn new_from_json(json: String) -> Result<Self, Box<dyn Error>> {
        Ok(serde_json::from_str(&json)?)
    }

    pub fn as_json(&self) -> String {
        // This shouldn't be able to fail
        serde_json::to_string(&self).expect("Failed to serialize to json")
    }
}

#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug, Copy, Clone)]
#[repr(u8)]
pub enum Kind {
    SetMetadata = 0,
    TextNote = 1,
    RecommendServer = 2,
    ContactList = 3,
    EncryptedDirectMessage = 4,
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub struct Tag(Vec<String>);

impl Tag {
    pub fn new(kind: &str, content: &str, recommended_relay_url: &str) -> Self {
        Self(vec![
            kind.into(),
            content.into(),
            recommended_relay_url.into(),
        ])
    }

    pub fn kind(&self) -> &str {
        &self.0[0]
    }

    pub fn content(&self) -> &str {
        &self.0[1]
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tags_deser_without_recommended_relay() {
        //The TAG array has dynamic length because the third element(Recommended relay url) is optional
        let sample_event = r#"{"id":"2be17aa3031bdcb006f0fce80c146dea9c1c0268b0af2398bb673365c6444d45","pubkey":"f86c44a2de95d9149b51c6a29afeabba264c18e2fa7c49de93424a0c56947785","created_at":1640839235,"kind":4,"tags":[["p","13adc511de7e1cfcf1c6b7f6365fb5a03442d7bcacf565ea57fa7770912c023d"]],"content":"uRuvYr585B80L6rSJiHocw==?iv=oh6LVqdsYYol3JfFnXTbPA==","sig":"a5d9290ef9659083c490b303eb7ee41356d8778ff19f2f91776c8dc4443388a64ffcf336e61af4c25c05ac3ae952d1ced889ed655b67790891222aaa15b99fdd"}"#;
        let ev_ser = Event::new_from_json(sample_event.into()).unwrap();
        assert_eq!(ev_ser.as_json(), sample_event);
    }

    #[test]
    fn test_text_note(){
        let content = "This is a test";
        let keypair_str = hex::decode("5cee490ee759ca71e82140f86f8f192df14ba053cc0ca0ba335019791bdb1c66de3aaa95154ec33f50ebd841c65f329f53e3060610b323ec5033c24c438c2e7f").unwrap();
        let kp = Keys::new(&keypair_str[..]).unwrap();
        let test = Event::new_textnote(content, &kp).unwrap();
        println!("{:?}", test.as_json());
    }
}
